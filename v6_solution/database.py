from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .config import settings

DATABASE_URL = (
    f"postgresql://{settings.database_username}:"
    f"{settings.database_password}@{settings.database_host}:"
    f"{settings.database_port}/{settings.database_name}"
    )

# Running engine for ORM translation (python to SQL)
database_engine = create_engine(DATABASE_URL)
# Template for the connection
SessionTemplate = sessionmaker(
    autocommit=False, autoflush=False, bind=database_engine)

# Dependency : Create and close session on-demand
def get_db():
    db = SessionTemplate()
    try:
        yield db
    finally:
        db.close()
