from fastapi import FastAPI, status, HTTPException # FastAPI Module + status + HTTPException
from time import sleep # Sleep make the server waits
import psycopg2 # PostgreSQL adapter
from psycopg2.extras import RealDictCursor # Add names of fields to cursor
from pydantic import BaseModel  # Schema
from typing import Optional # For optional quantity only
from datetime import datetime

# Database Connection
while True:
    try: # Try / except to catch the errors
        connexion = psycopg2.connect(
                host='localhost',
                database='onetomany',
                #database='postgres',
                user='postgres',
                password='api', # Good password
                cursor_factory=RealDictCursor)
        cursor= connexion.cursor()
        print('Database connection: successful')
        break # If connection succeed, break the loop
    except Exception as error:
        print('Database connection: failed')
        print('Error: ', error)
        sleep(2) # If connection fails, retry in 2 seconds

# FastAPI server
app = FastAPI() # API instance name @app.get("/posts")

# Select all blogpost records
@app.get("/published_house")
def get_blogpost():
    # Writing the SQL query
    cursor.execute("SELECT * published_house")
    # Retrieving all the posts (list/array)
    database_published_house = cursor.fetchall()
    return {"data": database_published_house}

# Select a blogpost record from its id
@app.get('/published_house/{id_uri}')
def get_post(id_uri: int):
    cursor.execute("SELECT * FROM published_house WHERE id=%i" % id_uri)
    corresponding_post = cursor.fetchone()
    if not corresponding_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Not corresponding post was found with id:{id_uri}")
    return {"data": corresponding_post}

# Pydantic schema for BlogPost Body validation
class BlogPost(BaseModel):  
    title: str
    content: str
    writer_id: int
# Insert a new blogpost record
@app.post('/blogpost', status_code=status.HTTP_201_CREATED)
def create_post(post_body: BlogPost):
    try:
        cursor.execute("INSERT INTO blogpost (title, content, writer_id) VALUES(%s, %s, %s) RETURNING *;",
                    (post_body.title, post_body.content, post_body.writer_id))
        new_post = cursor.fetchone()
        connexion.commit(); # Save the changes to the Database
        #return new_post
        return {"data" :new_post}
    except psycopg2.error.ForeignKeyViolation as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        detail=f"Foreign key violation with writer_id:{post_body.writer_id}")

# Update a blogpost record from its id
@app.put('/blogpost/{id_uri}')
def update_post(id_uri: int, post_body: BlogPost):
        try:
            cursor.execute("UPDATE blogpost SET title=%s, content=%s, writer_id=%s WHERE id=%s RETURNING *;",
                    (post_body.title, post_body.content, post_body.writer_id, str(id_uri)))
            updated_post = cursor.fetchone()
            connexion.commit()
            if not updated_post:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Not corresponding post was found with id:{id_uri}")
            return {"data": updated_post}
        except psycopg2.errors.ForeignKeyViolation as err:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Foreign key violation with writer_id:{post_body.writer_id}")

# Delete a blogpost record from its id
@app.delete('/blogpost/{id_uri}', status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id_uri:int):
    cursor.execute("DELETE FROM blogpost WHERE id=%i RETURNING *;" % id_uri)
    deleting_post = cursor.fetchone()
    connexion.commit()  # Save the changes to the Database
    if not deleting_post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_uri}")
    return {"data": deleting_post}
