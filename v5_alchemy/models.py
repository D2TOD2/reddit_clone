from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.sql.sqltypes import TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.orm import relationship


# The base class will be the base for all the models we will create
Base = declarative_base()

class User(Base):
    __tablename__= "user"
    id = Column(Integer, primary_key=True, nullable=False)
    email = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    created_at = Column(TIMESTAMP(timezone=True),
                        server_default=text('now()'), nullable=False)
    

class Blogpost(Base):
    __tablename__= "blogpost"
    id = Column(Integer, primary_key=True, nullable=False)
    title =Column(String, nullable=False)
    content =Column(String, nullable=False)
    published =Column(Boolean, server_default=text('true'), nullable=False)
    created_at =Column(TIMESTAMP(timezone=True),
                        server_default=text('now()'), nullable=False)
    writer_id =Column(Integer, ForeignKey("user.id", ondelete="CASCADE"),
                      nullable=False)
    writer = relationship(User)
    