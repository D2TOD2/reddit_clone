
from typing import List
from .database import get_db, database_engine
from fastapi import Depends, FastAPI, status, HTTPException, Response, status
from sqlalchemy.orm import Session
from . import models, schemas

# Create the tables if they don't exists yet
models.Base.metadata.create_all(bind=database_engine)

app = FastAPI() # Run the Server

# Fetch all data from User table
@app.get('/users', response_model=List[schemas.User_Response])
def get_users(db: Session = Depends(get_db)):
    all_users = db.query(models.User).all()
    return all_users

@app.get('/users/{uri_id}', response_model=schemas.User_Response)
def get_user(uri_id: int, db: Session = Depends(get_db)):
    corresponding_user = db.query(models.User).filter(models.User.id == uri_id).first()
    
    if not corresponding_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Not corresponding user was found with id:{uri_id}")
    return corresponding_user


@app.get('/blogpost', response_model=List[schemas.Blogpost_Response])
def get_blogpost(db: Session = Depends(get_db)):
    all_blogpost = db.query(models.Blogpost).all()
    return all_blogpost