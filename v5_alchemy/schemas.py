from datetime import datetime
from pydantic import BaseModel, EmailStr

# UserPy
# Pydantic schema for User Body validation
class UserPy(BaseModel):
    email: EmailStr # str is too general (random text)
    password: str
    
# Pydantic schema User Response
class User_Response(UserPy):
    id: int
    created_at: datetime
    class Config: # Important for pydantic model/schema translation
        orm_mode = True
        
        
class BlogpostPy(BaseModel):
    title: str
    content: str
    # published: bool = True
    writer_id: int
    
class Blogpost_Response(BlogpostPy):
    id: int
    created_at: datetime
    published: bool
    writer: User_Response
    class Config:
        orm_mode = True