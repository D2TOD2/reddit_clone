from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
# Database URL
DATABASE_URL='postgresql://postgres:api@localhost:5432/sqlalchemy'
# Running engine for ORM translation (python to SQL)
database_engine = create_engine(DATABASE_URL)

# Template for the connection
SessionTemplate = sessionmaker(autocommit=False,
                               autoflush=False, bind=database_engine)

# Dependency : Create and close session on-demand
def get_db():
    db = SessionTemplate()
    try:
        yield db
    finally:
        db.close()