from jose import jwt, JWTError
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from ..schemas import Token

SERVER_KEY = "d030dc110ab2bf19f44839dcdf114fccf51bf63b05ad0daec0cf401cffa8c34f"
ALGORITHM = "HS256"


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth")

# encode token function
def generate_token(user_id: int):
    payload = {"user_id": user_id}
    encoded_jwt = jwt.encode(payload, SERVER_KEY, algorithm=ALGORITHM)
    return Token(access_token=encoded_jwt, token_type="bearer")


#decode token function
def decode_token(provided_token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(provided_token, SERVER_KEY, algorithms=[ALGORITHM])
        decoded_id: str = payload.get("user_id")
    except JWTError:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"}
            
        )
    return decoded_id    