from operator import mod
from statistics import mode
from typing import List

from .database import get_db, database_engine
from fastapi import Depends, FastAPI, HTTPException, Response, status
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from . import models, schemas
from .utilities import hash_manager, jwt_manager
from fastapi.security.oauth2 import OAuth2PasswordRequestForm

# Create the tables if they don't exists yet
models.Base.metadata.create_all(bind=database_engine)


app = FastAPI()  # Run the Server

################# BlogPosts EndPoints ##################
#
# Get all the posts endpoint
#
@app.get('/posts', response_model=List[schemas.BlogPost_Response])
def get_posts(db: Session = Depends(get_db)):
    all_posts = db.query(models.BlogPost).all()
    return all_posts


#
# Create new blogpost endpoint
#
@app.post('/posts', response_model=schemas.BlogPost_Response)
def create_post(post_body: schemas.BlogPost, db: Session = Depends(get_db)):
    new_post = models.BlogPost(**post_body.dict())
    db.add(new_post)  # Execute the INSERT
    db.commit()  # Save the modificatio to the DB
    db.refresh(new_post)  # to replace "RETURNING *"
    return new_post


#
# get blogpost with ID
#
@app.get('/posts/{uri_id}', response_model=schemas.BlogPost_Response)
def get_post(uri_id: int, db: Session = Depends(get_db)):
    corresponding_post = db.query(models.BlogPost).filter(
        models.BlogPost.id == uri_id).first()
    if not corresponding_post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{uri_id}"
        )
    return corresponding_post


#
# DELETE blogpost endpoint
#
@app.delete('/posts/{uri_id}')
def delete_post(uri_id: int, db: Session = Depends(get_db)):
    # find blogpost with id provided             
    query_post = db.query(models.BlogPost).filter(
        models.BlogPost.id == uri_id)
    if not query_post.first():  # Check if this post exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{uri_id}"
        )
    query_post.delete()  # DELETE the post
    db.commit()  # Save changes to the DB
    return Response(status_code=status.HTTP_204_NO_CONTENT)


#
# UPDATE blogpost endpoint
#
@app.put('/posts/{uri_id}', response_model=schemas.BlogPost_Response)
def update_post(uri_id: int, post_body: schemas.BlogPost, db: Session = Depends(get_db)):
    # find blogpost with id provided                
    query_post = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id)
    if not query_post.first():  # make sure it exists
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{uri_id}"
        )
    query_post.update(post_body.dict())  # Update the posts
    db.commit()  # Save changes to DB
    return query_post.first()


############### USERS ENDPOINTS ####################
#
# GET all the users endpoint
#
@app.get('/users', response_model=List[schemas.User_Response])
def get_users(db: Session = Depends(get_db)):
    all_users = db.query(models.User).all()
    return all_users

# GET my user
@app.get('/users/me', response_model=schemas.User_Response)
def get_user_me(
    user_id: int = Depends(jwt_manager.decode_token),
    db:Session=Depends(get_db)):
    corresponding_user = db.query(models.User).filter(
        models.User.id == user_id ).first()
    return corresponding_user





#
# GET user from ID endpoint
#
@app.get('/users/{uri_id}', response_model=schemas.User_Response)
def get_user(uri_id:int, db:Session=Depends(get_db)):
    corresponding_user = db.query(models.User).filter(models.User.id == uri_id).first()
    if not corresponding_user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not user found with the id:{uri_id}"
        )
    return corresponding_user


#
# Create new user
#
@app.post('/users', status_code=status.HTTP_201_CREATED, response_model=schemas.User_Response)
def create_user(user_body: schemas.UserCreate, db: Session = Depends(get_db)):
    # hashing the password
    pwd_hashed = hash_manager.hash_pass(user_body.password)
    user_body.password = pwd_hashed # override the plaintext with hased password
    
    new_user = models.User(**user_body.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


incorrectException = HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password ",
            headers={"WWW-Authenticate": "Bearer"}
        )

# /auth endpoint
@app.post('/auth', status_code=status.HTTP_202_ACCEPTED, response_model=schemas.Token)
def auth_user(user_credentials: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    
    #get corresponding user from database
    corresponding_user: models.User = db.query(models.User).filter(models.User.email == user_credentials.username).first()
    
    #if not corresponding user found, raise an exception
    if not corresponding_user:
        raise incorrectException
    
    # check the password
    pass_valid = hash_manager.verify_password(user_credentials.password, corresponding_user.password)
    
    #if password invalid, raise an exception
    if not pass_valid:
        raise incorrectException
    
    jwt = jwt_manager.generate_token(corresponding_user.id)
    return jwt
