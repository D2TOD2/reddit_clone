from datetime import datetime
from pydantic import BaseModel, EmailStr

# Users
class UserCreate(BaseModel):
    email: EmailStr  # str is too general (random text)
    password: str
    
class User_Credentials(UserCreate):
    pass 

class User_Response(BaseModel):
    id: int
    email: EmailStr
    created_at: datetime
    class Config:  # Important for pydantic model/schema translation
        orm_mode = True

class BlogPost(BaseModel):  # Pydantic schema for POST Body validation
    title: str
    content: str
    published: bool = True
    writer_id: int

class BlogPost_Response(BlogPost):  # Pydantic schema for POST Body validation
    id: int
    created_at: datetime
    class Config:
        orm_mode = True


class Token(BaseModel):
    acces_token: str
    token_type: str
    
