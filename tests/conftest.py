import pytest
from fastapi.testclient import TestClient
from v6_solution.main import app
from v6_solution.models import Base
from v6_solution.database import get_db
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# Bad Practice to put it here
DATABASE_URL = 'postgresql://postgres:api@localhost/sqlalchemy_test'

# Running engine for ORM translation (python to SQL)
database_engine = create_engine(DATABASE_URL)
# Template for the connection
TestingSessionTemplate = sessionmaker(autocommit=False, autoflush=False, bind=database_engine)

# Dependency : Create and close session on-demand
def override_get_db():
    db = TestingSessionTemplate()
    try:
        yield db
    finally:
        db.close()

@pytest.fixture()
def session():
    #Clean DB deleting previous tables
    Base.metadata.drop_all(bind=database_engine)
    #Create tables
    Base.metadata.create_all(bind=database_engine)
    app.dependency_overrides[get_db] = override_get_db
    
    
@pytest.fixture()
def client(session):
    yield TestClient(app)    
    
    
    
@pytest.fixture()
def create_user(client):
    user_credentials = {"email":"test.user@domain.lu", "password":"1234"}
    #post request to create a new user
    res = client.post("/users", json=user_credentials)
    #response json including ONLY id, email and created_at
    new_user = res.json()
    #adding passsword to response
    new_user['password'] = user_credentials['password']
    #returning new_user including id, email, created_at and plain text password
    return new_user

    

@pytest.fixture
def user_token(create_user, client):
    res = client.post("/auth", data={"username": create_user['email'],
                      "password": create_user['password']})
    return res.json().get("access_token")

@pytest.fixture
def authorized_client(client, user_token):
    client.headers = {**client.headers,
                      "Authorization": f"Bearer {user_token}"}
    return client
    

