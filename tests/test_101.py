import pytest


def test_101():
    print("testing the 101")
    assert 100 + 1 == 101

#Exercise tested function
def total_with_tip(bill, percentage):
    return bill + bill * percentage/100

def test_tip_100():
    print("Tipping 20% on 100€")
    assert total_with_tip(100,20) == 120
    
# Made with Pierres
    
def total_with_tip(bill, percentage):
    if(bill < 0 or percentage < 0):
        raise Exception("Bill and Precentage have to be positive")
    return bill + bill*percentage/100

# Parametrization
@pytest.mark.parametrize("num1, num2, expectation", 
                         [(10, 20, 12),
                          (100, 20, 120),
                          (0, 0, 0)])
def test_tip_bulk(num1, num2, expectation):
    assert total_with_tip(num1, num2) == expectation

